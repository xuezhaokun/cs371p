// ---------
// Hello.c++
// ---------

// http://isocpp.github.io/CppCoreGuidelines/CppCoreGuidelines.html
// http://en.cppreference.com

#include <iostream> // cout, endl

int main () {
    using namespace std;
    cout << "Nothing to be done." << endl;
    return 0;}

/*
Developed in 1978 by Bjarne Stroustrup of Denmark.
C++ is procedural, object-oriented, statically typed, and not garbage collected.

C++98
C++03
C++11
C++14
C++17
C++20



% which g++-8
/usr/local/bin/g++-8



% g++-8 --version
g++-8 (Homebrew GCC 8.2.0) 8.2.0



% g++-8 -pedantic -std=c++14 -Wall -Weffc++ -Wextra Hello.c++ -o Hello



% ./Hello
Nothing to be done.
*/
