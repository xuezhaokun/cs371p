// -----------
// Fri,  1 Feb
// -----------

/*
three ways of providing input
1. just give the input, expect the read to eventually fail
2. tell me up front
3. tell me at the end, a sentinel
*/

/*
you have to pass at least one of the HackerRank tests to get credit
*/

/*
fork and clone the public code repo
make your own private code repo (*** invite the graders ***)
when you're done, submit the GitLab URL to Canvas
*/

/*
run the downing/gcc Docker image to develop your code
test your code on HackerRank
*/

/*
git commit with the right kind of message can automatically resolve an issue
*/

/*
unit tests
    use Google Test
    are called white box tests
    are code
*/

/*
continuous integration
automatic testing of git pushes
*/

/*
acceptance tests
    are black box tests
    two files: a file of pairs, a file of triples
confirm conformance of your input with checktestdata on the CS machines
push your acceptance tests to the public test repo via merge request
*/
