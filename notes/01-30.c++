// -----------
// Wed, 30 Jan
// -----------

/*
every time we do a quiz,      later that day I remove the code
every time we do an exercise, later that day I change the code to 1234

Piazza / Canvas
labs sessions, next one is tonight
Zoom office hours, 3:30pm on some days, I'll announce it on Piazza

unit tests
coverage
exceptions
*/

/*
Password: 6738

1. run the code and confirm success
2. identify and fix the tests
3. run the code and confirm failure
4. fix the code
5. run the code and confirm success
*/

// pretend that C++ doesn't have exceptions

// 1. return a special value

int f (...) {
    ...
    if (<something wrong>)
        return <special value>
    ...}

int g (...) {
    ...
    int i = f(...);
    if (i == <special value>)
        <something wrong>
    ...}

// 2. pass in a special variable, pass by reference, mutable

int f (..., int& e2) {
    ...
    if (<something wrong>) {
        e2 = -1;
        return ...}
    ...}

int g (...) {
    ...
    int e = 0
    int i = f(..., e);
    if (e == -1)
        <something wrong>
    ...}

// 3. global variable

int h = 0;

int f (...) {
    ...
    if (<something wrong>) {
        h = -1;
        return ...}
    ...}

int g (...) {
    ...
    int i = f(...);
    if (h == -1)
        <something wrong>
    ...}

/*
assertions are not recoverable
three ways above are ignorable
exceptions are recoverable and not ignorable
*/

/*
1. throw and no try block,        go to higher block
2. throw and the right try block, handle exception
*/

int f (...) {
    ...
    if (<something wrong>)
        throw E(...);
    ...}

int g (...) {
    ...
    try {
        ...
        int i = f(...);
        ...}
    catch (E e) {
        <something wrong>}
    ...}

/*
3. throw and the wrong try block, go to higher block
*/

int f (...) {
    ...
    if (<something wrong>)
        throw Y(...);
    ...}

int g (...) {
    ...
    try {
        ...
        int i = f(...);
        ...}
    catch (E e) {
        <something wrong>}
    ...}
