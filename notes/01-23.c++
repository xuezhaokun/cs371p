// -----------
// Wed, 23 Jan
// -----------

/*
ACM  is 80 years old
ICPC is 40 years old

local contest every couple of weeks
in the fall there's a regional contest

140 regions in the world

our region, SCUSA, 3 states, 25 schools, 65 teams of 3 students
we take 4 teams, 12 students

2014: 2nd, Rice 1st, Morocco
2015: 2nd, Rice 1st, Thailand, our 4 teams in the top 7
2016: 1st, 2nd, 3rd, 4th, South Dakota
2017: 1st, Beijing
2018: 1st, Portugal

get to know somebody
name, contact info
when are they graduating
why are they taking this class, why now
exchange expectations

google "cs371p fall 2018 final entry"
*/
