// -----------
// Mon, 28 Jan
// -----------

/*
operator overloading
*/

int i = 2;
int j = 3;
int k = (i << j);

// cout is an ostream, from the library iostream
cout << i;

// cin is an istream, from the library iostream
cin >> i;

i + j * k // j * k goes first, * has higher precedence than +
i = j = k // j = k goes first, = is right associative
i - j - k // i - j goes first, - is left  associative

/*
number of args for *   is 2
number of args for ++  is 1
number of args for ? : is 3
number of args for ()  is n
*/

/*
overload operators

1. can't change the precedence
2. can't change the associativity
3. can't change the arity (number of arguments)
4. can't overload a NEW token, has to be an already existing token
5. can't define an operator on built-in types, only user-defined types
*/

i = j + k;
i = 2 + 3;
j + k;     // no
i = 2;     // 2 is an r-value
2 = i;     // no, 2 is an r-value

(i + j) = k; // no, + produces an r-value

i += j;
i += 2;
2 += 2; // no, += requires an l-value on the left

k = (i += j); // legal in C, C++, Java; illegal in Python

(i += j) = k; // legal in C++; illegal in C, Java, Python

int i = 2;
int j = ++i; // increment, return incremented
cout << j;   // 3

int i = 2;
int j = i++; // increment, return un-incremented
cout << j;   // 2

++i; // just increment
i++; // just increment, slower for user-defined types

for (I i = 0; i < s; ++i)
    ...

++i = j; // yes, pre-increment returns an l-value
i++ = j; // no,  post-increment returns an r-value

++2; // no, pre-increment  needs an l-value
2++; // no, post-increment needs an l-value

++++i; // yes, increments by 2; illegal in C, Java, Python
i++++; // no, post-increment produces an r-value, but needs an l-value

++i++; // try it

i+++; // no, doesn't parse
